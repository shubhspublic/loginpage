import React from "react";

function Label(props) {
  return (
    <label className="sizeFix title" id={props.id}>
      {props.name}
    </label>
  );
}

export default Label;
