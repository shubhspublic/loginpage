import React from "react";

function Input(props) {
  return (
    <input
      className={props.className}
      type="text"
      placeholder={props.placeholder}
      value={props.value}
      onChange ={props.onChange}
      id={props.id}
    ></input>
  );
}

export default Input;
