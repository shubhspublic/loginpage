import React from "react";

function Button(props) {
  return (
    <button type={props.type} onClick={props.click} className={props.className} onMouseOver={props.onmouseover} onMouseOut={props.onmouseout} id={props.id}>
      {props.name}
    </button>
  );
}

export default Button;
