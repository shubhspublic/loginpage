import React, { useState } from "react";
import Button from "./button";
import Input from "./input";
import Label from "./label";

function App() {
  const [title, setTitle] = useState("Hello ");
  const [visible, setVisible] = useState("visible");
  const [hover, setHover] = useState("sizeFix btn");
  const [fname, setFName] = useState("");
  const [lname, setLName] = useState("");
  const [email, setEmail] = useState("");

  function handleFChange(event) {
    setFName(event.target.value);
    setTitle("Hello "+event.target.value+" "+lname+" "+email)
  }

  function handleLChange(event) {
    setLName(event.target.value);
    setTitle("Hello "+fname+" "+event.target.value+" "+email)
  }

  function handleEChange(event) {
    setEmail(event.target.value);
    setTitle("Hello "+fname+" "+lname+" "+event.target.value)
  }

  function handleClick(event) {
    setTitle("Hello " + fname+" "+lname);
    setVisible("hidden");
    event.preventDefault()
  }

  function onMouseOver() {
    setHover("sizeFix btnHover");
  }

  function onMouseOut() {
    setHover("sizeFix btn");
  }

  return (
    <div>
      <form className="loginBox" onSubmit={handleClick}>
        <Label name={title} />
        <Input
          className="sizeFix"
          onChange={handleFChange}
          visibility={visible}
          placeholder="Enter Firstname"
          value={fname}
        />
        <Input
          className="sizeFix"
          onChange={handleLChange}
          visibility={visible}
          placeholder="Enter Lastname"
          value={lname}
        />
         <Input
          className="sizeFix"
          onChange={handleEChange}
          visibility={visible}
          placeholder="Enter Email"
          value={email}
        />
        <Button
          className={hover}
          onChange={handleEChange}
          type="submit"
          onmouseover={onMouseOver}
          onmouseout={onMouseOut}
          name="Submit"
        />
      </form>
    </div>
  );
}

export default App;
